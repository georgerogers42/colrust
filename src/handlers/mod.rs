use std::sync::Arc;
use std::fmt;
use std::fmt::{Display, Formatter};
use std::error::Error;
use std::collections::HashMap;

use iron::prelude::*;
use iron::middleware::Handler;
use iron::{headers, status};
use router::{Router, NoRoute};
use mount::Mount;
use staticfile::Static;
use logger::Logger;

use tera;
use tera::{Tera, Context};

use models;
use models::Article;

pub fn home(tpl: &Tera, articles: &Vec<Article>, _req: &mut Request) -> IronResult<Response> {
    let mut ctx = Context::new();
    ctx.add("articles", &articles);
    let mut resp = Response::with((status::Ok, tpl.render("index.html", &ctx).unwrap()));
    resp.headers.set(headers::ContentType::html());
    Ok(resp)
}

pub fn article(tpl: &Tera,
               articles: &HashMap<String, Article>,
               req: &mut Request)
               -> IronResult<Response> {
    let slug = req.extensions
        .get::<Router>()
        .unwrap()
        .find("slug")
        .unwrap();
    let mut ctx = Context::new();
    match articles.get(slug) {
        Some(article) => {
            ctx.add("article", article);
            let mut resp = Response::with((status::Ok, tpl.render("article.html", &ctx).unwrap()));
            resp.headers.set(headers::ContentType::html());
            Ok(resp)
        }
        None => Err(IronError::new(NoRoute, status::NotFound)),
    }
}

#[derive(Debug)]
pub enum LoadError {
    Articles(models::Error),
    Templates(tera::Error),
}

impl Display for LoadError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Error for LoadError {
    fn description(&self) -> &str {
        match self {
            &LoadError::Articles(ref e) => e.description(),
            &LoadError::Templates(ref e) => e.description(),
        }
    }
}

pub type App = Box<Handler>;

pub fn app() -> Result<App, LoadError> {
    let tpl = Arc::new(try!(Tera::new("templates/*.html").map_err(LoadError::Templates)));
    let articles = Arc::new(try!(models::load_articles("articles/*.html")
                                     .map_err(LoadError::Articles)));
    let article_map = Arc::new(models::article_map(&articles));
    let mut r = Router::new();
    {
        let tpl = tpl.clone();
        let articles = articles.clone();
        r.get("/",
              move |r: &mut Request| home(&tpl, &articles, r),
              "index");
    }
    {
        let tpl = tpl.clone();
        let article_map = article_map.clone();
        r.get("/articles/:slug",
              move |r: &mut Request| article(&tpl, &article_map, r),
              "articles");
    }
    let mut m = Mount::new();
    m.mount("/", r);
    m.mount("/static/", Static::new("static/"));
    let (lb, la) = Logger::new(None);
    let mut c = Box::new(Chain::new(m));
    c.link_before(lb);
    c.link_after(la);
    Ok(c)
}
