#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate glob;
extern crate iron;
extern crate router;
extern crate mount;
extern crate logger;
extern crate staticfile;
extern crate chrono;
extern crate tera;
extern crate env_logger;

use std::env::args;

use iron::prelude::*;

mod handlers;
mod models;

fn main() {
    env_logger::init().unwrap();
    let argv: Vec<String> = args().collect();
    let m = handlers::app().unwrap();
    println!("Starting on: {}", &argv[1]);
    Iron::new(m).http(&argv[1]).unwrap();
}
