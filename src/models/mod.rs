use std::fmt;
use std::error;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io;
use chrono::prelude::*;
use serde_json;
use glob;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    Ser(serde_json::Error),
    Pattern(glob::PatternError),
    Glob(glob::GlobError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            &Error::IO(ref e) => e.description(),
            &Error::Ser(ref e) => e.description(),
            &Error::Pattern(ref e) => e.description(),
            &Error::Glob(ref e) => e.description(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Meta {
    pub title: String,
    pub slug: String,
    pub author: String,
    pub date: DateTime<Utc>,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Article {
    pub meta: Meta,
    pub contents: String,
}

impl Article {
    pub fn new(meta: Meta, contents: String) -> Article {
        Article {
            meta: meta,
            contents: contents,
        }
    }
}

pub fn article_map(articles: &[Article]) -> HashMap<String, Article> {
    let mut amap = HashMap::new();
    for article in articles.iter() {
        amap.insert(article.meta.slug.clone(), article.clone());
    }
    amap
}

pub fn load_articles(root: &str) -> Result<Vec<Article>, Error> {
    let mut articles = vec![];
    let fnames = try!(glob::glob(root).map_err(Error::Pattern));
    for fname in fnames {
        let file = io::BufReader::new(try!(File::open(try!(fname.map_err(Error::Glob)))
                                               .map_err(Error::IO)));
        articles.push(try!(load_article(file)));
    }
    articles.sort_by(|a, b| b.meta.date.cmp(&a.meta.date));
    Ok(articles)
}

fn load_article<R: BufRead>(r: R) -> Result<Article, Error> {
    let mut mstr = "".to_owned();
    let mut lines = r.lines();
    for line in &mut lines {
        let line = try!(line.map_err(Error::IO));
        if line == "" {
            break;
        }
        mstr.push_str(&line);
        mstr.push_str("\n");
    }
    let meta = try!(serde_json::from_str(&mstr).map_err(Error::Ser));
    let mut contents = "".to_owned();
    for line in &mut lines {
        let line = try!(line.map_err(Error::IO));
        contents.push_str(&line);
        contents.push_str("\n");
    }
    Ok(Article::new(meta, contents))
}
