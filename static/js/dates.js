require(["jquery"], function($) {
  "use strict";
  $(function() {
    for(let x of $(".date")) {
      let $x = $(x);
      $x.text(new Date($x.text()));
    }
  });
});
